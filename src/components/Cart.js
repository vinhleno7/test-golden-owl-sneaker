import React from "react";
import LogoCard from "../assets/img/nike.png";
import ListData from "../data/shoes.json";
import { useSelector, useDispatch } from "react-redux";
import btnDelete from "../assets/img/trash.png";
import {
  addMoreShoe,
  decreaseShoe,
  deleteShoe
} from "../redux/actions/shoeAction";

const Cart = () => {
  const { shoe } = useSelector((state) => state);
  const dispatch = useDispatch();
  const [objDel, setObjDel] = React.useState("");

  const handleAddMoreShoe = (shoe) => {
    dispatch(addMoreShoe(shoe));
  };
  const handleDecreaseShoe = (shoe) => {
    setObjDel(shoe.id);
    dispatch(decreaseShoe(shoe));
    setObjDel("");
  };
  const handleDeleteShoe = (shoe) => {
    setObjDel(shoe.id);
    setTimeout(() => {
      dispatch(deleteShoe(shoe));
      setObjDel("");
    }, 3000);
  };

  return (
    <div className="card">
      <div className="card-top">
        <img src={LogoCard} alt="card-top-logo" className="card-top-logo" />
      </div>
      <div className="card-title">
        Your cart
        <span className="card-title-amount">
          {shoe.sum ? `$${shoe.sum}` : "$0.00"}
        </span>
      </div>

      <div className="card-body">
        <div className="cart-items">
          <div>
            {shoe.count > 0
              ? shoe.shoes.map((shoeCart) => (
                  <div
                    key={shoeCart.id}
                    className={
                      objDel === shoeCart.id
                        ? "cart-list-leave-active cart-list-leave-to cart-item"
                        : " animate fade cart-item"
                    }
                  >
                    <div className="cart-item-left">
                      <div
                        className="cart-item-image animate fade delay-1"
                        style={{ backgroundColor: `${shoeCart.color}` }}
                      >
                        <div className="cart-item-image-block  animate fade delay-1">
                          <img src={shoeCart.image} alt="shoe-img" />
                        </div>
                      </div>
                    </div>
                    <div className="cart-item-right">
                      <div className="cart-item-name animate fade delay-3">
                        {shoeCart.name}
                      </div>
                      <div className="cart-item-price animate fade delay-4">{`$${shoeCart.price}`}</div>
                      <div className="cart-item-actions animate fade delay-5">
                        <div className="cart-item-count">
                          <div
                            className="cart-item-count-button"
                            onClick={() => handleDecreaseShoe(shoeCart)}
                          >
                            -
                          </div>
                          <div className="cart-item-count-number">
                            {shoeCart.countBuy ? shoeCart.countBuy : 1}
                          </div>
                          <div
                            className="cart-item-count-button"
                            onClick={() => handleAddMoreShoe(shoeCart)}
                          >
                            +
                          </div>
                        </div>
                        <div
                          className="cart-item-remove"
                          onClick={() => handleDeleteShoe(shoeCart)}
                        >
                          <img
                            src={btnDelete}
                            className="cart-item-remove-icon"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                ))
              : "Your cart is empty"}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cart;
