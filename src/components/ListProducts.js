import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import LogoCard from "../assets/img/nike.png";
import ListData from "../data/shoes.json";
import { getShoe } from "../redux/actions/shoeAction";

const ListProducts = () => {
  const { shoe } = useSelector((state) => state);

  const myCart = JSON.parse(localStorage.getItem("shoes"));

  const checkShoe = (shoeJson) => {
    if (myCart) return myCart.shoes.filter((item) => item.id === shoeJson.id);
    else return;
  };

  const dispatch = useDispatch();
  const handleAddToCart = (shoeJson) => {
    dispatch(getShoe(shoeJson));
  };

  return (
    <div className="card">
      <div className="card-top">
        <img src={LogoCard} alt="card-top-logo" className="card-top-logo" />
      </div>
      <div className="card-title">Our Products</div>

      <div className="card-body">
        <div className="shop-items">
          {ListData.shoes.map((shoeJson) => (
            <div key={shoeJson.id} className="shop-item">
              <div
                className="shop-item-image"
                style={{ background: `${shoeJson.color}` }}
              >
                <img src={shoeJson.image} alt="shoe-img" />
              </div>
              <div className="shop-item-name">{shoeJson.name}</div>
              <div className="shop-item-description">
                {shoeJson.description}
              </div>
              <div className="shop-item-bottom animate fade ">
                <div className="shop-item-price">{`$${shoeJson.price} `}</div>
                <div
                  className={`${
                    myCart &&
                    myCart.shoes.filter((item) => item.id === shoeJson.id)
                      .length > 0
                      ? "shop-item-button inactive"
                      : "shop-item-button"
                  }`}
                  onClick={() => handleAddToCart(shoeJson)}
                >
                  {myCart &&
                  myCart.shoes.filter((item) => item.id === shoeJson.id)
                    .length > 0 ? (
                    <div className="shop-item-button-cover   ">
                      <div className="shop-item-button-cover-check-icon   "></div>
                    </div>
                  ) : (
                    <p className="button-text  ">ADD TO CART</p>
                  )}
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ListProducts;
