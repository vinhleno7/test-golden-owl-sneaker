import CartPage from "./pages/Cart";
import { useEffect } from "react";
import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import { getListCart } from "./redux/actions/shoeAction";

function App() {
  const { shoe } = useSelector((state) => state);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getListCart());
  }, [dispatch]);

  return <CartPage />;
}

export default App;
