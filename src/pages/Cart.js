import React from "react";
import ListProducts from "../components/ListProducts";
import CartCard from "../components/Cart";

const Cart = () => {
  return (
    <div className="main-content">
      <ListProducts />
      <CartCard />
    </div>
  );
};

export default Cart;
