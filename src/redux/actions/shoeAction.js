export const SHOE_TYPES = {
  UPDATE_SHOE: "UPDATE_SHOE",
  GET_SHOES: "GET_SHOES",
  ADD_SHOE: "ADD_SHOE",
  DELETE_SHOE: "DELETE_SHOE",
  ADD_MORE_SHOE: "ADD_MORE_SHOE",
  DECREASE_SHOE: "DECREASE_SHOE"
};

export const getShoe = (shoe) => (dispatch) => {
  try {
    dispatch({
      type: SHOE_TYPES.ADD_SHOE,
      payload: shoe
    });
  } catch (error) {
    console.log(error);
  }
};

export const getListCart = () => (dispatch) => {
  const listCart = localStorage.getItem("shoes");
  const parseListCart = JSON.parse(listCart);
  try {
    dispatch({
      type: SHOE_TYPES.GET_SHOES,
      payload: parseListCart
    });
  } catch (error) {
    console.log(error);
  }
};

export const addMoreShoe = (shoe) => (dispatch) => {
  try {
    dispatch({
      type: SHOE_TYPES.ADD_MORE_SHOE,
      payload: shoe
    });
  } catch (error) {
    console.log(error);
  }
};

export const decreaseShoe = (shoe) => (dispatch) => {
  try {
    dispatch({
      type: SHOE_TYPES.DECREASE_SHOE,
      payload: shoe
    });
  } catch (error) {
    console.log(error);
  }
};
export const deleteShoe = (shoe) => (dispatch) => {
  try {
    dispatch({
      type: SHOE_TYPES.DELETE_SHOE,
      payload: shoe
    });
  } catch (error) {
    console.log(error);
  }
};
