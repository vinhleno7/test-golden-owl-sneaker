import { SHOE_TYPES } from "../actions/shoeAction";

const initialState = {
  statusAdd: false,
  statusRemove: false,
  shoes: [],
  count: 0,
  sum: 0
};

const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOE_TYPES.GET_SHOES:
      return {
        ...state,
        shoes: action.payload.shoes,
        count: action.payload.count,
        sum: action.payload.sum
      };

    case SHOE_TYPES.ADD_MORE_SHOE:
      let countTemp = action.payload.countBuy ? action.payload.countBuy : 1;

      state.shoes.forEach((item) => {
        if (item.id === action.payload.id) {
          console.log(" + EXIST RIGHT", action.payload.id);
          countTemp = countTemp + 1;
        }
      });
      localStorage.setItem(
        "shoes",
        JSON.stringify({
          ...state,

          shoes: state.shoes.map((item) =>
            item.id === action.payload.id
              ? {
                  ...item,
                  id: action.payload.id,
                  image: action.payload.image,
                  name: action.payload.name,
                  description: action.payload.description,
                  price: action.payload.price,
                  color: action.payload.color,
                  countBuy: countTemp
                }
              : item
          ),
          count: state.count + 1,
          sum: Math.round((state.sum + action.payload.price) * 100) / 100
        })
      );
      return {
        ...state,

        shoes: state.shoes.map((item) =>
          item.id === action.payload.id
            ? {
                ...item,
                id: action.payload.id,
                image: action.payload.image,
                name: action.payload.name,
                description: action.payload.description,
                price: action.payload.price,
                color: action.payload.color,
                countBuy: countTemp
              }
            : item
        ),
        count: state.count + 1,
        sum: Math.round((state.sum + action.payload.price) * 100) / 100
      };

    // ~~~~~~~~~~~~
    case SHOE_TYPES.DECREASE_SHOE:
      let countTemp2 = action.payload.countBuy ? action.payload.countBuy : 1;
      let boolAppear = false;
      state.shoes.forEach((item) => {
        if (item.id === action.payload.id) {
          console.log("- EXIST RIGHT", action.payload.id);
          boolAppear = true;
        }
      });
      countTemp2 = boolAppear ? countTemp2 - 1 : countTemp2;

      if (countTemp2 > 0) {
        localStorage.setItem(
          "shoes",
          JSON.stringify({
            ...state,

            shoes: state.shoes.map((item) =>
              item.id === action.payload.id
                ? {
                    ...item,
                    id: action.payload.id,
                    image: action.payload.image,
                    name: action.payload.name,
                    description: action.payload.description,
                    price: action.payload.price,
                    color: action.payload.color,
                    countBuy: countTemp2
                  }
                : item
            ),
            count: state.count - 1,
            sum: Math.round((state.sum - action.payload.price) * 100) / 100
          })
        );
        return {
          ...state,

          shoes: state.shoes.map((item) =>
            item.id === action.payload.id
              ? {
                  ...item,
                  id: action.payload.id,
                  image: action.payload.image,
                  name: action.payload.name,
                  description: action.payload.description,
                  price: action.payload.price,
                  color: action.payload.color,
                  countBuy: countTemp2
                }
              : item
          ),
          count: state.count - 1,
          sum: Math.round((state.sum - action.payload.price) * 100) / 100
        };
      } else {
        localStorage.setItem(
          "shoes",
          JSON.stringify({
            ...state,

            shoes: state.shoes.filter((item) => item.id !== action.payload.id),
            count: state.count - 1,
            sum: Math.round((state.sum - action.payload.price) * 100) / 100
          })
        );
        return {
          ...state,
          statusRemove: true,

          shoes: state.shoes.filter((item) => item.id !== action.payload.id),
          count: state.count - 1,
          sum: Math.round((state.sum - action.payload.price) * 100) / 100
        };
      }
    // ~~~~~~~~~~~~~~~~
    case SHOE_TYPES.ADD_SHOE:
      localStorage.setItem(
        "shoes",
        JSON.stringify({
          ...state,
          shoes: [...state.shoes, action.payload],
          count: state.shoes.length + 1,
          sum: Math.round((state.sum + action.payload.price) * 100) / 100
        })
      );
      return {
        ...state,
        statusAdd: true,
        shoes: [...state.shoes, action.payload],
        count: state.shoes.length + 1,
        sum: Math.round((state.sum + action.payload.price) * 100) / 100
      };
    // ~~~~~~~~~~~~~~~~
    case SHOE_TYPES.DELETE_SHOE:
      localStorage.setItem(
        "shoes",
        JSON.stringify({
          ...state,

          shoes: state.shoes.filter((item) => item.id !== action.payload.id),
          count: action.payload.countBuy
            ? state.count - action.payload.countBuy
            : state.count - 1,
          sum: action.payload.countBuy
            ? Math.round(
                (state.sum - action.payload.price * action.payload.countBuy) *
                  100
              ) / 100
            : Math.round((state.sum - action.payload.price) * 100) / 100
        })
      );
      return {
        ...state,
        statusRemove: true,
        shoes: state.shoes.filter((item) => item.id !== action.payload.id),
        count: action.payload.countBuy
          ? state.count - action.payload.countBuy
          : state.count - 1,
        sum: action.payload.countBuy
          ? Math.round(
              (state.sum - action.payload.price * action.payload.countBuy) * 100
            ) / 100
          : Math.round((state.sum - action.payload.price) * 100) / 100
      };
    default:
      return state;
  }
};

export default shoeReducer;
