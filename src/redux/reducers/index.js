import { combineReducers } from "redux";
import shoe from "./shoeReducer";

export default combineReducers({
  shoe
});
